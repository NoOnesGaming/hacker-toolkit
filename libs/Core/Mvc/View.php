<?php

namespace Core\Mvc;

class View
{
    public function includeHeader()
    {
        include('header.phtml');
    }

    public function includeFooter()
    {
        include('footer.phtml');
    }

    public function getPartial($partial)
    {
        include("app/partials/{$partial}.phtml");
    }

    public function render($view, $path = null, $args = array())
    {
        extract($args);
        ob_start();
        include("app/views/{$path}/{$view}.phtml");
        ob_end_flush();
    }
}