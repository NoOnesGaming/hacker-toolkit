<?php

namespace Core\Mvc;

class Controller
{
    public $defaultViewPath = null;
    public $view;
    public function __construct()
    {
        $this->view = new View;
    }
    public function getModel($model)
    {
        $modelPath = 'app/models/' . $model . '.php';
        if (!file_exists($modelPath)) {
            return null;
        } else {
            require_once($modelPath);
            return new $model;
        }
    }
}