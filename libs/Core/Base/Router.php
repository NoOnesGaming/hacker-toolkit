<?php

/**
 * @package Core\Base
 * @author Ole Aass
 */
namespace Core\Base;

/**
 * Class Router
 *
 * @todo Error handling
 * @todo Better comments on methods
 */
class Router
{
    public $route = array();
    public $controller = null;
    public $view = null;
    public $args = array();

    /**
     * @param array $route
     * @param bool $redirect
     */
    public function __construct($route, $redirect = false)
    {
        $route = trim($route, '/');
        $this->route = (empty($route)) ? array() : explode('/', $route);
        $this->parse();
    }

    public function parse()
    {
        if (empty($this->route)) {
            $this->controller = 'DefaultController';
            $this->action = 'index';
        } else {
            $this->controller = ucfirst($this->route[0]) . 'Controller';
            $this->action = (isseT($this->route[1])) ? $this->route[1] : 'index';
            $this->args = array_slice($this->route, 2);
        }
    }

    public function redirect()
    {
        if ($realpath = realpath("app/controllers/{$this->controller}.php")) {
            require_once($realpath);
            $controller = new $this->controller;
            $controller->{$this->action}($this->args);
        }
    }
}