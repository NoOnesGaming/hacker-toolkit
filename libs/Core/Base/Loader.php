<?php

/**
 * @package Core\Base
 * @author Ole Aass
 */
namespace Core\Base;

/**
 * Dependencies
 *
 * @uses UnexpectedValueException
 */
use UnexpectedValueException;

/**
 * Class Loader
 *
 * @todo Proper error handling
 * @todo Better comments on some methods
 */
class Loader
{
    /**
     * @var null|string Absolute path to the vendor folder
     */
    public $libraryPath = null;

    /**
     * Class constructor
     *
     * @param string $libraryPath
     * @return \Core\Base\Loader
     */
    public function __construct($libraryPath)
    {
        $this->libraryPath = $libraryPath;
        spl_autoload_register(array($this, 'loadClass'));
    }

    /**
     * Class loader
     *
     * @param string $class
     */
    public function loadClass($class)
    {
        $path = "{$this->libraryPath}/" . str_replace('\\', DIRECTORY_SEPARATOR, $class) . ".php";
        if ($realpath = realpath($path)) {
            require_once($realpath);
        }

    }
}