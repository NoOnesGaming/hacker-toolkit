delimiter $$

CREATE DATABASE `hackertoolkit` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */$$

delimiter $$

CREATE TABLE `hashcracker` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `plaintext` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hash` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `algorithm` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `hash_UNIQUE` (`hash`),
  KEY `plaintext_INDEX` (`plaintext`),
  KEY `algorithm_INDEX` (`algorithm`)
) ENGINE=InnoDB AUTO_INCREMENT=12226752 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci$$


delimiter $$

CREATE TABLE `sqli` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci$$

delimitier $$

INSERT INTO `hackertoolkit`.`sqli` (`id`, `username`, `password`, `name`, `email`) VALUES
('1', 'jdoe', '22c14f311a60486b36f79f3bc962be66', 'Jane Doe', 'jdoe@hackertoolkit'),
('2', 'esmith', '12f9c46a2f99fa00a451f9d9db9d511c', 'Eric Smith', 'esmith@hackertoolkit')$$