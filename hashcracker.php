<?php include('header.phtml'); ?>
<div class="container-fluid">
    <div class="row-fluid">
        <form method="post" class="form-horizontal">
            Hash:
            <input type="text" name="hash" class="input-xxlarge" placeholder="enter hash string">
            Algorithm:
            <select name="algorithm">
                <option value="md5_nosalt">MD5(password)</option>
                <option value="md5_pwdsalt">MD5(password.salt)</option>
                <option value="md5_saltpwd">MD5(salt.password)</option>
            </select>
            <input type="submit" class="btn btn-primary" value="Search">
        </form>
        <hr>
    </div>
</div>
<?php include('footer.phtml'); ?>