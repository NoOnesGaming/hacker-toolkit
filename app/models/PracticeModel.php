<?php

use Core\Mvc\Model;

class PracticeModel extends Model
{
    public function sqliBypassAuthentication($credentials, $level)
    {
        switch ($level) {
            case 1:
                $username = $credentials['username'];
                $password = md5($credentials['password']);

                try {
                    $query = "SELECT * FROM sqli WHERE username='{$username}' AND password='{$password}'";
                    $stmt = $this->pdo->prepare($query);
                    $stmt->execute();
                    if (!$user = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        $error = $stmt->errorInfo();
                        throw new PDOException($error[2]);
                    } else {
                        return array('success' => true, 'user' => $user);
                    }
                } catch (PDOException $e) {
                    return array('success' => false, 'error' => $e->getMessage());
                }
                break;
        }
    }

    public function sqliExtractData($search, $level)
    {
        switch ($level) {
            case 1:
                try {
                    $query = "SELECT * FROM sqli WHERE username='{$search}'";
                    $stmt = $this->pdo->prepare($query);
                    $stmt->execute();
                    if (!$users = $stmt->fetchAll(PDO::FETCH_ASSOC)) {
                        $error = $stmt->errorInfo();
                        throw new PDOException($error[2]);
                    } else {
                        return array('success' => true, 'users' => $users);
                    }
                } catch (PDOException $e) {
                    return array('success' => false, 'error' => $e->getMessage());
                }
        }
    }
}