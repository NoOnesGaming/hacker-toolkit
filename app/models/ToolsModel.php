<?php

use Core\Mvc\Model;

class ToolsModel extends Model
{
    const VALIDATE_HASH_MD5 = '/[\w\d]+{,32}/';
    const VALIDATE_HASH_SHA1 = '/[\w\d]{,40}/';

    public function getTotalHashCount()
    {
        $query = 'SELECT COUNT(*) AS total FROM hashcracker';
        $result = $this->pdo->query($query);
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row['total'];
    }

    public function getTotalHashAlgorithmGroups()
    {
        $query = 'SELECT DISTINCT algorithm AS name, COUNT(*) AS hash_count FROM hashcracker GROUP BY algorithm ORDER BY algorithm';
        $result = $this->pdo->query($query);
        $row = $result->fetchAll(PDO::FETCH_ASSOC);
        return $row;
    }

    public function getUniquePlaintextCount()
    {
        $query = 'SELECT COUNT(DISTINCT plaintext) AS total FROM hashcracker';
        $result = $this->pdo->query($query);
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row['total'];
    }

    public function crackHash($hash)
    {
        /*switch ($algorithm) {
            case 'md5_nosalt':
            case 'md5_pwdsal':
            case 'md5_saltpwd':
                if (!preg_match(self::VALIDATE_HASH_MD5, $hash)) {
                    return null;
                }
                break;
            case 'sha1_nosalt':
            case 'sha1_pwdsal':
            case 'sha1_saltpwd':
                if (!preg_match(self::VALIDATE_HASH_SHA1, $hash)) {
                    return null;
                }
                break;
            default:
                return null;
                break;
        }*/
        $bound = array();
        $hashes = array();
        for ($i = 0; $i < sizeof($hash); $i++) {
            $bound[":hash{$i}"] = trim($hash[$i]);
            $placeholders[] = ":hash{$i}";
            $hashes[trim($hash[$i])] = array('plaintext' => '', 'hash' => $hash[$i], 'algorithm' => '');
        }

        $placeholders = implode(',', $placeholders);
        $query = 'SELECT plaintext,hash,algorithm FROM hashcracker WHERE hash IN (' . $placeholders . ')';

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($bound);

            while (false !== ($match = $stmt->fetch(PDO::FETCH_ASSOC))) {
                $hashes[$match['hash']] = $match;

                $matches = array_values($hashes);
            }
            return $matches;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function addHashes($words, $maxSize = 20000)
    {
        $words = array_slice($words, 0, $maxSize);
        var_dump($words);

        $insert = '';
        foreach ($words as $word) {
            $word = trim($word);
            $hashes = array(
                // MD5 variants
                'MD5' => md5($word),
                'MD5x2' => md5(md5($word)),
                'MD5x3' => md5(md5(md5($word))),
                'MD5(SHA1)' => md5(sha1($word)),

                // SHA1 variants
                'SHA1' => sha1($word),
                'SHA1x2' => sha1(sha1($word)),
                'SHA1x3' => sha1(sha1(sha1($word))),
                'SHA1(MD5)' => sha1(md5($word)),
            );
            foreach ($hashes as $algorithm => $hash) {
                $word = htmlentities($word, ENT_QUOTES);
                $insert .= "('{$word}','{$hash}','{$algorithm}'),";
            }
        }
        $query = 'INSERT INTO hashcracker (plaintext, hash, algorithm) VALUES' . trim($insert, ',') . ' ON DUPLICATE KEY UPDATE id=id';
        try {
            $stmt = $this->pdo->prepare($query);
            if (!$stmt->execute()) {
                $error = $stmt->errorInfo();
                throw new PDOException($error[2]);
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}