<?php

use Core\Mvc\Controller;

class PracticeController extends Controller
{
    public function sqli(array $args = array())
    {
        list($type, $level) = $args;
        switch($type) {
            case 'bypass':
                $credentials = (isset($_POST)) ? $_POST : array();
                if (isset($_POST) && !empty($_POST)) {
                    $model = $this->getModel('PracticeModel');
                    $result = $model->sqliBypassAuthentication($credentials, $level);
                    if ($result['success']) {
                        $name = $result['user']['name'];
                        $data['message'] = <<<STRING
Login successful!<br />Welcome back <strong>$name</strong>!
STRING;

                    } else {
                        $data['message'] = 'Login failed. Please try again!<br /><strong>Error:</strong>' . $result['error'];
                    }
                }
                $data['level'] = $level;
                break;
            case 'extract':
                $formData = (isset($_POST)) ? $_POST : array();
                if (isset($_POST) && !empty($_POST)) {
                    $model = $this->getModel('PracticeModel');
                    $result = $model->sqliExtractData($_POST['search'], $level);
                    if ($result['success']) {
                        $data['users'] = $result['users'];
                    } else {
                        $data['message'] = 'No match';
                    }
                }
                $data['level'] = $level;
                break;
        }
        $this->view->render($type, 'practice/sqli', $data);
    }

    public function cmdi(array $args = array())
    {
        $data['level'] = $args[0];
        switch ($args[0]) {
            case 1: // Difficulty level 1
                if (isset($_POST) && !empty($_POST)) {
                    exec('ping ' . $_POST['target'], $output);
                    $data['output'] = $output;
                }
                break;
        }
        $this->view->render('index', 'practice/cmdi', $data);
    }
}