<?php

use Core\Mvc\Controller;

class ToolsController extends Controller
{
    public function index($args = array())
    {
        $this->hashcracker($args);
    }

    public function hashcracker()
    {
        $model = $this->getModel('ToolsModel');
        if (isset($_POST) && !empty($_POST)) {
            $action = mb_strtolower($_POST['action']);

            switch ($action) {
                case 'crack':
                    $hash = (isset($_POST['hash'])) ? explode("\n", $_POST['hash']) : '';

                    if (empty($hash)) {

                    } else {
                        $data['matches'] = $model->crackHash($hash);
                        $i = 0;
                        foreach ($data['matches'] as $match) {
                            if (!empty($match['plaintext'])) {
                                $i++;
                            }
                        }
                        $data['matchCount'] = $i;
                    }
                    break;
                case 'add':
                    $words = (isset($_POST['words'])) ? explode("\n", $_POST['words']) : '';

                    if (empty($words)) {

                    } else {
                        $model->addHashes($words);
                    }
            }
        }
        $data['stats']['algorithmGroups'] = $model->getTotalHashAlgorithmGroups();
        $data['stats']['totalUniqueHashes'] = number_format($model->getTotalHashCount());
        $data['stats']['totalAlgorithmGroups'] = sizeof($data['stats']['algorithmGroups']);

        $this->view->render('index', 'tools/hashcracker', $data);
    }

    public function vulnscan()
    {
        $target = (isset($_POST['target'])) ? $_POST['target'] : '';
        if (empty($target)) {
            $data = array(
                'success' => false,
                'error' => 'No target was specified'
            );
        } else {
            $data['vulnNames'] = array(
                'sqli'  => 'SQL Injection',
                'xss'   => 'Cross-Site Scripting',
                'csrf'  => 'Cross-Site Request Forgery',
                'rfi'   => 'Remote File Inclusion',
                'lfi'   => 'Local File Inclusion',
                'fpd'   => 'Full Path Disclosure',
                'cgi'   => 'PHP-CGI exploit'
            );
            $vs = new Core\Tools\VulnerabilityScanner;
            $vs->init($target);
            $vs->buildQuerieStrings();
            $data['vulnerabilities'] = $vs->run();
        }
        $this->view->render('index', 'tools/vulnscan', $data);
    }
}