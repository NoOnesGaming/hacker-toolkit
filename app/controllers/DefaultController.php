<?php

use Core\Mvc\Controller;

class DefaultController extends Controller
{
    public function index()
    {
        $this->view->render('index', 'default');
    }
}