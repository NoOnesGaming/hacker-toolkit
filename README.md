Hacker Toolkit
===

This is a project that will grow slowly over time. The idea is to create a browser based application that provides tools related to hacking.
This includes, but is not limited to; Hash cracker, vulnerability scanner, pastebin, information storage and wordlists
