<?php
session_start();
require_once('libs/Core/Base/Loader.php');
new Core\Base\Loader(dirname(__FILE__) . '/libs');

$router = new Core\Base\Router($_SERVER['REQUEST_URI']);
$router->redirect();